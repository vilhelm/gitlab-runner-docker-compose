# gitlab-runner-docker-compose

Run using docker-compose.

```bash
docker-compose up -d
```

Perform setup.

```bash
docker exec -it gitlab-runner-docker-compose_runner_1 gitlab-runner register
```
